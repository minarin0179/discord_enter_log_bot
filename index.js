const Eris = require("eris");
require('date-utils')

//botのトークン
var bot = new Eris("ODMzNjk5MTcxNzMwODQ5ODMy.YH2I2w.ELHuwu3BVIZ1fG9xq1dHmBh5Ia0");
//ログを表示するテキストチャンネルのID
var log_channelID="848470894913519657";
//AFKチャンネルのID
var AFK_cchannelID="848514632544550912";
//稼働状態
var status=true;

//ステータスの更新
function status_online(){
    bot.editStatus("online",{name : "利用可能"});
}

function status_idle(){
    bot.editStatus("idle",{name : "利用停止"});
}

function status_dnd(){
    bot.editStatus("dnd",{name : "利用不可"});
}

//立ち上げ時の処理
bot.on("ready", () => {
    console.log("入退室管理を開始しました");
    status_online();
    //bot.createMessage(log_channelID,"> クライアントにより起動されました、これより入退室管理を開始します");
});

//メッセージによるbot操作のコマンド
bot.on("messageCreate", (msg) => {

    //botのメッセージは無視
    if (msg.author.bot) return;

    //botの利用停止再開
    if (msg.content === '!stop') {
        status =false;
        //bot.createMessage(msg.channel.id,"一時停止します");
        status_idle();
    }
    if (msg.content === '!start') {
        status =true;
        //bot.createMessage(msg.channel.id,"再開します");
        status_online();
    }

    //botの稼働状況確認
    if(msg.content ==='!status'){
        if(status){
            bot.createMessage(msg.channel.id,"botは現在利用可能です");
        }else{
            bot.createMessage(msg.channel.id,"botは現在停止中です");
        }
    }

    //helpの表示
    if (msg.content === '!help') {
        bot.createMessage(msg.channel.id,"!status - botの稼働状況を確認できます\n!stop - botの利用を一時停止します\n!start - botの利用を再開します");
    }
  });

//ボイスチャンネルに参加したときの処理
bot.on("voiceChannelJoin", (member, newChannel) => {
    var dt=new Date();
    if(newChannel.id==AFK_cchannelID) return;
    if(status){
        if(newChannel.voiceMembers.size<=1){
            bot.createMessage(log_channelID,"<@"+member.user.id + "> が <#" + newChannel.id + "> で待ってるよ "+dt.toFormat("(HH24:MI)"));
        }else{
            bot.createMessage(log_channelID,"<@"+member.user.id + "> が <#" + newChannel.id + "> に合流したよ "+dt.toFormat("(HH24:MI)"));
        }
    }
});

//ボイスチャンネルを移動したときの処理
bot.on("voiceChannelSwitch", (member, newChannel) => {
    var dt=new Date();
    if(newChannel.id==AFK_cchannelID) return;
    if(status){
        if(newChannel.voiceMembers.size<=1){
            bot.createMessage(log_channelID,"<@"+member.user.id + "> が <#" + newChannel.id + "> で待ってるよ "+dt.toFormat("(HH24:MI)"));
        }else{
            bot.createMessage(log_channelID,"<@"+member.user.id + "> が <#" + newChannel.id + "> に合流したよ "+dt.toFormat("(HH24:MI)"));
        }
    }
});

process.on("beforeExit",function(){
    status_dnd();
});

// Discord に接続
bot.connect();